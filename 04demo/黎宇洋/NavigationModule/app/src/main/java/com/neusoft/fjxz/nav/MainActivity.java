package com.neusoft.fjxz.nav;



import android.os.Bundle;
import android.view.Window;


import com.example.androidmvcframework.ApplicationConfigContext;

public class MainActivity extends ApplicationConfigContext {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //隐藏标题栏
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }


}